package com.demo.controller;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.classes.Address;
import com.demo.classes.Person;
import com.demo.repository.MyRepo;
import com.demo.repository.MyRepository;





@RestController
public class MyController {
	@Autowired
	private MyRepository myrepsitory;
//	@Autowired
////	private MyService service;
	@Autowired
	private MyRepo myrepo;
	@PostMapping("/test/AllMethod/requests/forALl")
    public String createItem(@RequestBody Person item,Address address) {
		myrepsitory.save(item);
		System.out.println(item.getName());
		myrepo.save(address);
		System.out.println(address.getHomeNo());
        return " Customer Added Succesfully "+ " Name :" +item.getName();
    }
//	@Transactional
//	@PostMapping("/test/AllMethod/requests/Method")
//	public void saveValues(@RequestBody Person person,Address address) {
//		myrepsitory.save(person);
//		myrepo.save(address);
//	}
//	@PostMapping("/test/AllMethod/address")
//    public String createItem1(@RequestBody Address address) {
//		myrepo.save(address);
//        return " Address Added Succesfully ";
//    }
//	@RequestMapping("/getData")
//	public String getDataFromPostman(Model model, @Param("Keyword") Integer Keyword) {
//		List<Person> person = ((MyRepository) service).getallPerson(Keyword);
//		model.addAttribute("person", person);
//		String result="";
//		
//		for(Person p : person) {
//			result = result+"|"+p.getName()+"|"+p.getAge()+"|"+p.getEmail();
//		}
//		return result;
//		 
//}
	@RequestMapping("/getData/{id}")
	public Optional<Person> getPerson(@PathVariable("id") int id){
		return myrepsitory.findById(id);
	}

}
