package com.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.classes.Address;

public interface MyRepo extends JpaRepository<Address, Integer> {

}
