package com.demo.classes;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CUSTOMER")
public class Person {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "person_name")
	private String name;
	@Column(name = "person_age")
	private String age;
	@Column(name = "person_emailId")
	private String email;
	@OneToMany(mappedBy = "user")
	private List<Address> address;
	public Person(){
		
	}
	public Person(String name, String age, String email) {
		super();
		this.name = name;
		this.age = age;
		this.email = email;
//		this.address = address;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
//	public Address getAddress() {
//		return address;
//	}
//	public void setAddress(Address address) {
//		this.address = address;
//	}
	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", email=" + email +"]";
	}
//	public String getHomeNo() {
//		return address.getHomeNo();
//	}
//	public String getStreet() {
//		return address.getStreet();
//	}
//	public String getCity() {
//		return address.getCity();
//	}
	
	
	
}
